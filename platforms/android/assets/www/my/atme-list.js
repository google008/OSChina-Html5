define(['text!my/atme-list.html', "../base/openapi", '../base/util', "../base/login/login", "../tweet/tweet-list", "../news/news-list", "../question/question-list"],
	function(viewTemplate, OpenAPI, Util, Login, TweetList, NewsList, QuestionList) {
		return Piece.View.extend({
			login: null,
			tweetList: null,
			newsList: null,
			questionList: null,
			id: 'my_atme-list',
			events: {
				"click .editBtn": "editQuestion",
				"click .tweetContent": "tweetDetail",
				"click .tweetListImg": 'goToUserInfor'
			},
			goToUserInfor: function(imgEl) {
				Util.imgGoToUserInfor(imgEl);
			},
			editQuestion: function() {
				tweetList = new TweetList();

				tweetList.navigate("tweet/tweet-issue?from=atme-list", {
					trigger: true
				});
			},
			tweetDetail: function(el) {
				tweetList = new TweetList();
				newsList = new NewsList();
				questionList = new QuestionList();

				var $target = $(el.currentTarget);
				var id = $target.attr("data-objectId");
				var objType = $target.attr("data-objType");

				/*-------------------传到详情页面参数-----------------------*/
				var fromType; //1-软件；2-问答；3-博客；4-咨询；5-代码；7-翻译；
				var checkDetail;
				var com; //1-新闻；2-问答；3-动弹；5-博客；

				if (objType == 4 || objType == 16) {
					fromType = 4;
					checkDetail = "news/news-detail";
					com = 1;
					//判断返回  到我的空间还是  不同的DETAUIL详情
					from = null;
					newsList.navigate("news/news-detail?id=" + id + "&fromType=" + fromType + "&checkDetail=" + checkDetail + "&com=" + com + "&from=" + from, {
						trigger: true
					});
				} else if (objType == 100 || objType == 101) {
					tweetList.navigate("tweet/tweet-detail?id=" + id, {
						trigger: true
					});
				} else if (objType == 3 || objType == 18) {
					fromType = 3;
					checkDetail = "news/news-blog-detail";
					com = 5;
					//判断返回  到我的空间还是  不同的DETAUIL详情
					from = null;

					newsList.navigate("news/news-blog-detail?id=" + id + "&fromType=" + fromType + "&checkDetail=" + checkDetail + "&com=" + com + "&from=" + from, {
						trigger: true
					});
				} else if (objType == 2 || objType == 17) {
					fromType = 2;
					checkDetail = "question/question-detail";
					com = 2;
					from = null;

					questionList.navigate("question/question-detail?id=" + id + "&fromType=" + fromType + "&checkDetail=" + checkDetail + "&com=" + com + "&from=" + from, {
						trigger: true
					});
				}
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				login = new Login();

				var checkLogin = Util.checkLogin();

				if (checkLogin === false) {
					login.show();
				} else {
					var me = this;
					var user_message = Piece.Store.loadObject('user_message');
					var user_token = Piece.Store.loadObject('user_token');
					var atmePrompt = $('.atmePrompt').html();
					if (atmePrompt != '') {
						Util.AjaxNoLoading(OpenAPI.clear_notice, "GET", {
							access_token: user_token.access_token,
							type: 1,
							dataType: 'jsonp'
						}, 'json', function(data, textStatus, jqXHR) {
							var NavPrompt = Piece.Session.loadObject('Prompt');
							if (NavPrompt != null) {
								NavPrompt.referCount = 0;
								Piece.Session.saveObject('Prompt', NavPrompt);
							}
							Util.loadList(me, 'my-atme-list', OpenAPI.my_list, {
								'catalog': 2,
								'access_token': user_token.access_token,
								'user': user_message.id,
								'pageSize': OpenAPI.pageSize,
								'page': 1,
								'dataType': OpenAPI.dataType
							});
						}, null, null);
					} else {
						Util.loadList(me, 'my-atme-list', OpenAPI.my_list, {
							'catalog': 2,
							'access_token': user_token.access_token,
							'user': user_message.id,
							'pageSize': OpenAPI.pageSize,
							'page': 1,
							'dataType': OpenAPI.dataType
						});
					}



				}
			}
		}); //view define

	});