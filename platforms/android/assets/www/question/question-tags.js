define(['text!question/question-tags.html', "../base/openapi", '../base/util'],
	function(viewTemplate, OpenAPI, Util) {
		return Piece.View.extend({
			id: 'question_question-tags',
			events: {
				"click .homeBtn" :"goBackToHome",
				"click .questionContent": "goToQuestionList"
				
			},
			goToQuestionList: function(el) {
				var $target = $(el.currentTarget);
				var id = $target.attr("data-id");
				//checkDtail  toggle comment-list or xx-detail
				var checkDetail = "question/question-detail";
				//type    add  favorite
				var type = 2;
				//comment list
				var com = 2;
				this.navigate("question-detail?id=" + id  + "&checkDetail=" + checkDetail + "&fromType=" + type + "&com=" + com, {
					trigger: true
				});
			},
			goBackToHome: function() {
				window.history.back();
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				var tagsName = Util.request("tagsName")
				decodeURI(tagsName);
				$(".title").html(tagsName);
				Util.loadList(this, 'question-tags-list', OpenAPI.question_list, {
					'tag': tagsName,
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': OpenAPI.pageSize
				},true);
			}
		}); //view define

	});