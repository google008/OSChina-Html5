define(['text!../my/comment-detail.html', "../base/openapi", '../base/util', '../base/caretInsert/jquery.hammer.min'],
	function(viewTemplate, OpenAPI, Util) {
		return Piece.View.extend({
			menu: null,
			id: 'my_comment-detail',
			render: function() {
				$(this.el).html(viewTemplate);
				Piece.View.prototype.render.call(this);
				return this;
			},

			events: {
				"click .refreshBtn": "refreshContent",
				"click .homeBtn": "goBackToHome",
				"click .questionListUer": "gotoUser",
				"click #commentTweet": "commentTweet",
				"click .myListImg": "goToUserInfor",
				"click .tweetListImg": "goToUserInfor"

			},
			showOption: function(ev) {
				this.resetPosition();
				var me = this;

				$(window).unbind("resize", me.checkOptionsContent);

				//定义屏幕伸缩重新计算宽度
				$(window).resize(function() {
					me.checkOptionsContent();

				});
				$(".optionsContent").show();
				$("#commentListMasker").show();
				$("#commentListMasker").click(function() {
					$(".optionsContent").hide();
					$("#commentListMasker").hide();
				});

				//绑定点击转发
				$("#transpond").click(function() {
					me.transMessage(ev);
				});
				//绑定点击删除
				$("#del").click(function() {
					me.delMessage(ev);
				});


			},
			checkOptionsContent: function() {
				var me = this;
				if ($(".optionsContent").css("display") === "block") {
					me.resetPosition();
					$(".optionsContent").show();
				}
			},
			resetPosition: function() {
				$(".optionsContent").show();
				var left = ($(window).width() - $(".optionsContent").width()) / 2;
				var top = ($(window).height() - $(".optionsContent").height()) / 2;
				$(".optionsContent").css({
					"position": "absolute",
					"top": top,
					"left": left,
					"display": 'none'
				});
				$(".optionsContent").hide();
			},
			delMessage: function(el) {
				var checkLogin = Util.checkLogin();
				if (checkLogin === false) {
					var login = new Login();
					login.show();
					return;
				}
				$target = $(el.currentTarget);
				var userToken = Piece.Store.loadObject("user_token");
				var accessToken = userToken.access_token;
				var messageId = $target.attr("data-id");
				//option--hide
				$(".optionsContent").hide();
				$("#commentListMasker").hide();

				Util.Ajax(OpenAPI.comment_delete, "GET", {
					access_token: accessToken,
					id: messageId,
					catalog: 4,
					dataType: 'jsonp'
				}, 'json', function(data, textStatus, jqXHR) {
					if (data.error === "200") {
						new Piece.Toast("留言删除成功");
						// that.onShow();
						var id = Util.request("id");
						var friendname = Util.request("friendname");
						Util.reloadPage('my/comment-detail?id=' + id + "&friendname=" + friendname);
					} else {
						new Piece.Toast(data.error_description);
					}
				}, null, null);

			},
			transMessage: function(el) {
				var checkLogin = Util.checkLogin();
				if (checkLogin === false) {
					var login = new Login();
					login.show();
					return;
				}
				$target = $(el.currentTarget);
				var friendname = $target.attr("data-name");
				var spanId = "span" + $target.attr("data-id");
				//去除a标签
				$('#' + spanId).text($('#' + spanId).text());
				var content = $("#" + spanId).html();
				friendname = encodeURI(friendname);
				content = encodeURI(content);
				this.navigate("comment-transpond?friendname=" + friendname + "&content=" + content, {
					trigger: true
				});

			},
			goToUserInfor: function(imgEl) {
				Util.imgGoToUserInfor(imgEl);
			},
			commentTweet: function() {
				Util.checkLogin();
				var checkLogin = Util.checkLogin();
				if (checkLogin === false) {
					login.show();
				} else {
					this.commentPub();
				}
			},
			commentPub: function() {
				var commentCont = $("#commentCont").val();
				var me = this;
				commentCont = commentCont.replace(/(\s*$)/g, ""); //去掉右边空格
				if (commentCont === "" || commentCont === " ") {
					new Piece.Toast('请输入留言内容');
				} else {
					var userToken = Piece.Store.loadObject("user_token");
					var accessToken = userToken.access_token;
					var tweetId = Util.request("id");
					Util.Ajax(OpenAPI.comment_pub, "GET", {
						access_token: accessToken,
						id: tweetId,
						catalog: 4,
						content: commentCont,
						dataType: 'jsonp'
					}, 'json', function(data, textStatus, jqXHR) {
						//评论之后的一些评论框页面效果及刷新动态数据
						$("#bar-tab1").show();
						$("#bar-tab2").hide();
						$("#TweetEmotions").hide();
						$("#bar-tab2").attr({
							"style": "none"
						});
						$("#showEmotion").attr({
							"data-sign": "up"
						});
						if (data.error == 200) {
							new Piece.Toast('留言成功');
							$('.tab-inner').find('.footerBtn').hide();
							$('.footcomment').val("");
							$('.footcomment').attr("placeholder", "发送留言");
						} else {
							new Piece.Toast('留言失败');

						}

						me.onShow();
					}, null, null);

				}
			},

			gotoUser: function(el) {
				var $target = $(el.currentTarget);
				var id = $target.attr("data-id");
				this.navigateModule("common/common-seeUser?id=" + id, {
					trigger: true
				});

			},

			refreshContent: function() {
				this.onShow();
			},

			goBackToHome: function() {
				window.history.back();
			},

			onShow: function() {
				var me = this;

				$('.footcomment').click(function() {
					$('.tab-inner').find('.footerBtn').show();
					$('.footcomment').attr("placeholder", "");
				});
				var token = Piece.Store.loadObject("user_token");
				var accesstoken = token.access_token;
				var id = Util.request("id");
				Util.loadList(this, 'my-comment-detail', OpenAPI.comment_list, {
					'id': id,
					'catalog': 4,
					'dataType': OpenAPI.dataType,
					'page': 1,
					'pageSize': OpenAPI.pageSize,
					'access_token': accesstoken
				}, true);
				var friendname = Util.request("friendname")
				friendname = decodeURI(friendname);
				$('.senderTit').html(friendname);

				//绑定长按
				var me = this;
				me.longTouch(me);
			},
			longTouch: function(me) {
				var hammertime = $(me.el).find("#my-comment-detail").hammer();
				// console.info(hammertime);
				// on elements in the toucharea, with a stopPropagation
				hammertime.on("hold", ".myContent,.messageContent", function(ev) {
					me.showOption(ev);
				});

			}
		}); //view define

	});