define(['text!my/comment-list.html', "../base/openapi", '../base/util', "../base/login/login", "../tweet/tweet-list", '../base/caretInsert/jquery.hammer.min'],
	function(viewTemplate, OpenAPI, Util, Login, TweetList) {
		return Piece.View.extend({
			tweetList: null,
			login: null,
			id: 'my_comment-list',
			events: {
				"click .editBtn": "editQuestion",
				"click .messageContent": "messageDetail",
				"click .tweetListImg": 'goToUserInfor',
			},
			showOption: function(ev) {
				this.resetPosition();
				var me = this;

				$(window).unbind("resize", me.checkOptionsContent);

				//定义屏幕伸缩重新计算宽度
				$(window).resize(function() {
					me.checkOptionsContent();

				});
				$(".optionsContent").show();
				$("#commentListMasker").show();
				$("#commentListMasker").click(function() {
					$(".optionsContent").hide();
					$("#commentListMasker").hide();
				});
				//绑定点击回复
				$("#reply").click(function() {
					me.goMessage(ev);
				});
				//绑定点击转发
				$("#transpond").click(function() {
					me.transMessage(ev);
				});
				//绑定点击删除
				$("#del").click(function() {
					me.delMessage(ev);
				});


			},
			checkOptionsContent: function() {
				var me = this;
				if ($(".optionsContent").css("display") === "block") {
					me.resetPosition();
					$(".optionsContent").show();
				}
			},
			resetPosition: function() {
				$(".optionsContent").show();
				var left = ($(window).width() - $(".optionsContent").width()) / 2;
				var top = ($(window).height() - $(".optionsContent").height()) / 2;
				$(".optionsContent").css({
					"position": "absolute",
					"top": top,
					"left": left,
					"display": 'none'
				});
				$(".optionsContent").hide();
			},
			goMessage: function(el) {
				var checkLogin = Util.checkLogin();
				if (checkLogin === false) {
					var login = new Login();
					login.show();
					return;
				}
				$target = $(el.currentTarget);
				var userName = $target.attr("data-name");
				var userId = $target.attr("data-friendid");

				Backbone.history.navigate("common/comment-message?userOp=" + userName + "&userId=" + userId, {
					trigger: true
				});
			},
			delMessage: function(el) {
				var checkLogin = Util.checkLogin();
				if (checkLogin === false) {
					var login = new Login();
					login.show();
					return;
				}
				$target = $(el.currentTarget);
				var userToken = Piece.Store.loadObject("user_token");
				var accessToken = userToken.access_token;
				var userId = $target.attr("data-senderid");
				var friendId = $target.attr("data-friendid");
				//option--hide
				$(".optionsContent").hide();
				$("#commentListMasker").hide();

				Util.Ajax(OpenAPI.message_delete, "GET", {
					access_token: accessToken,
					user: userId,
					friend: friendId,
					dataType: 'jsonp'
				}, 'json', function(data, textStatus, jqXHR) {
					if (data.error === "200") {
						new Piece.Toast("留言删除成功");
						// that.onShow();
						Util.reloadPage('my/comment-list?reload=1')
					} else {
						new Piece.Toast(data.error_description);
					}
				}, null, null);

			},
			transMessage:function(el) {
				var checkLogin = Util.checkLogin();
				if (checkLogin === false) {
					var login = new Login();
					login.show();
					return;
				}
				$target = $(el.currentTarget);
				var friendname = $target.attr("data-name");
				// var content = $target.attr("data-content");
				var spanId = "span"+$target.attr("data-id");
				// $("#"+spanId+" a").contents().unwrap();
				//去除a标签
				$('#'+spanId).text($('#'+spanId).text());
				var content = $("#"+spanId).html();
				// var transContent = "@"+friendname+" "+content;
				// console.info(transContent);
				friendname = encodeURI(friendname);
				content = encodeURI(content);
				this.navigate("comment-transpond?friendname="+friendname+"&content="+content,{
					trigger:true
				});

			},
			goToUserInfor: function(imgEl) {
				Util.imgGoToUserInfor(imgEl);
			},
			editQuestion: function() {
				tweetList = new TweetList();

				tweetList.navigate("tweet/tweet-issue?from=comment-list", {
					trigger: true
				});
			},
			messageDetail: function(el) {
				if ($('#commentListMasker').get(0).style.display == 'block') {
					return;
				}

				var $target = $(el.currentTarget);
				var id = $target.attr("data-friendid");
				var friendname = $target.attr("data-name");
				console.info(friendname)
				friendname = encodeURI(friendname);
				this.navigate("comment-detail?id=" + id + "&friendname=" + friendname, {
					trigger: true
				});
			},
			render: function() {
				$(this.el).html(viewTemplate);

				Piece.View.prototype.render.call(this);
				return this;
			},
			onShow: function() {
				login = new Login();
				var reload = Util.request("reload") == 1 ? true : false;

				var checkLogin = Util.checkLogin();

				if (checkLogin === false) {
					login.show();
				} else {
					var me = this;
					var user_token = Piece.Store.loadObject('user_token');
					var commentPrompt = $('.commentPrompt ').html();
					if (commentPrompt != "") {
						Util.AjaxNoLoading(OpenAPI.clear_notice, "GET", {
							access_token: user_token.access_token,
							type: 2,
							dataType: 'jsonp'
						}, 'json', function(data, textStatus, jqXHR) {
							var NavPrompt = Piece.Session.loadObject('Prompt');
							if (NavPrompt != null) {
								NavPrompt.msgCount = 0;
								Piece.Session.saveObject('Prompt', NavPrompt);
							}
							var user_message = Piece.Store.loadObject('user_message');
							var user_token = Piece.Store.loadObject('user_token');
							Util.loadList(me, 'my-my-comment-list', OpenAPI.message_list, {
								'access_token': user_token.access_token,
								'dataType': OpenAPI.dataType,
								'page': 1,
								'pageSize': OpenAPI.pageSize
							});

						}, null, null);
					} else {

						var user_message = Piece.Store.loadObject('user_message');
						var user_token = Piece.Store.loadObject('user_token');
						Util.loadList(me, 'my-my-comment-list', OpenAPI.message_list, {
							'access_token': user_token.access_token,
							'dataType': OpenAPI.dataType,
							'page': 1,
							'pageSize': OpenAPI.pageSize
						}, reload);
					}


				}

				//绑定长按
				var me = this;
				me.longTouch(me);

			},
			longTouch: function(me) {
				var hammertime = $(me.el).find("#my-my-comment-list").hammer();
				// console.info(hammertime);
				// on elements in the toucharea, with a stopPropagation
				hammertime.on("hold", ".messageContent", function(ev) {
					me.showOption(ev);
				});

			}

		}); //view define

	});